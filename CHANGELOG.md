# 0.5 - Release Candidate
* Added Automated Settings Sync via Git
* Improved deployment - Each service is now fully independent from each other
* Added Airsonic - Airsonic is a free, web-based media streamer, providing ubiquitous access to your music.
* Added Bookstack - Simple & Free Wiki Software
* Added Ghost - Ghost is a platform for building and running a modern online publication
* Added Homedash - Home Server Dashboard Software
* Added Inventario - Home Inventory Management Software
* Added Jellyfin - The Free Software Media System
* Added Mashio - Home Brewery Management Software
* Added openLDAP - Open Source LDAP Server
* Added Zulip - Threaded Chat Software
* Various improvements and bug fixes

# 0.4

* Vastly improved initial setup
* Automated Grafana Configuration
* Added Cloud Bastion Server via Tinc VPN option
* Added individual service toggling via host vars
* Added The Lounge - IRC Bouncer
* Added Radarr - DVR
* Added Sonarr - DVR
* Added Kibitzr - IFTTT replacement
* Added BulletNotes - Note taking application
* Added Plex - Personal Media Server
* Upgraded Organizr to v2 - Dashboard
* Removed Koel - Rarely worked
* Removed Convos - Rarely worked

# 0.3

* Added Semi-automated Apple Health Import
* Added Automated HTTPS via LetsEncrypt
* Added Tor Onion Services - Remote access through any firewall
* Added OpenVPN - Remote device Ad filtering via piHole
* Added Mastodon - Federated social micro blogging
* Added Matomo - Web analytics
* Added Bitwarden - Password manager
* Added piHole - Network wide ad blocking

# 0.2

* Added Dasher / Amazon Dash Button support

# 0.1

* Initial release